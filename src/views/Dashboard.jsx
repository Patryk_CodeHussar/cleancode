/*!

=========================================================
* Black Dashboard React v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
import "./Dashboard";
// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// core components
import {
  chartExample1,
  chartExample2,
  chartExample3,
  chartExample4
} from "variables/charts.jsx";

import ModalLogin from "../components/ModalLogin.jsx";
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bigChartData: "data1",
      loged: localStorage.getItem('token') ? true : false
    };
  }
  setBgChartData = name => {
    this.setState({
      bigChartData: name
    });
  };

  afterLogin = () => {
    if (localStorage.getItem('token')) this.setState({ loged: true })
  }

  render() {
    return (
      <>
        <div className="content">
          {!this.state.loged && (<ModalLogin className="login-btn" afterLogin={this.afterLogin} buttonLabel="Log in" modalType="login" modalTitle="Log in"></ModalLogin>)}
          {!this.state.loged && (<ModalLogin className="login-btn" afterLogin={this.afterLogin} buttonLabel="Sign in" modalType="signIn" modalTitle="Sign in"></ModalLogin>)}
          <Row>
            <Col xs="12">
              <Card className="card-chart">
                <CardHeader>
                  <Row>
                    <Col className="text-left" sm="6">
                      <h5 className="card-category">Averange Points</h5>
                    </Col>
                    <Col sm="6">
                      <ButtonGroup
                        className="btn-group-toggle float-right"
                        data-toggle="buttons"
                      >
                        <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData === "data1"
                          })}
                          color="info"
                          id="0"
                          size="sm"
                          onClick={() => this.setBgChartData("data1")}
                        >
                          <input
                            defaultChecked
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Averange
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-single-02" />
                          </span>
                        </Button>
                      </ButtonGroup>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample1[this.state.bigChartData]}
                      options={chartExample1.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col lg="4">
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Total Points</h5>
                  <CardTitle tag="h3">
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample2.data}
                      options={chartExample2.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>

            <Col lg="8">
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Activity</h5>
                  <CardTitle tag="h3">

                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample4.data}
                      options={chartExample4.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>

            {/* <Col lg="12" md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Ranking</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                        <th>Project</th>
                        <th>Mark</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>WAT Plan</td>
                        <td>4.0</td>
                        <td>21.01.2020</td>
                      </tr>
                      <tr>
                        <td>One page</td>
                        <td>5.3</td>
                        <td>10.01.2020</td>
                      </tr>
                      <tr>
                        <td>Testowa</td>
                        <td>9.0</td>
                        <td>24.12.2019</td>
                      </tr>
                      <tr>
                        <td>Testowa2</td>
                        <td>0.2</td>
                        <td>20.12.2019</td>
                      </tr>
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col> */}
          </Row>
        </div>
      </>
    );
  }
}

export default Dashboard;
