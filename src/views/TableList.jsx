/*!

=========================================================
* Black Dashboard React v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col
} from "reactstrap";
import { useState, useEffect } from "react";
import { provider } from "../components/Provider";

function Tables(){

      const [data, setData]=useState();
      const [render, setrender]=useState("false");

      useEffect(() => {
        if(render=="false" && localStorage.getItem('token')){
          let headers = {
            // 'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          }
          fetch("https://cleancodespontanapi.azurewebsites.net/api/User",
          { headers: headers,})
          .then((resp) => resp.json()) // Transform the data into json
          .then(function(data) {
            // Create and append the li's to the ul
            setData(data);
            setrender("true");
            });
          
        }
      });

    if(render==="true"){
          return (
            <>
              <div className="content">
                <Row>
                  <Col md="12">
                    <Card>
                      <CardHeader>
                        <CardTitle tag="h4">Ranking</CardTitle>
                      </CardHeader>
                      <CardBody>
                        <Table >

                          <thead className="text-primary">
                            <tr>
                              <th>Login</th>
                              <th>Averange</th>
                              <th>Max point</th>
                              <th>Projects</th>
                            </tr>
                          </thead>

                          <tbody>
                            {data.map((user)=>{
                              return(
                              <tr>
                                <td>{user.username}</td>
                                <td>{user.overalMark}</td>
                                <td>{user.bestMark}</td>
                                <td>{user.numberOfProjects}</td>
                              </tr>
                              );
                            })} 
                            {console.log(data)}
                          </tbody>
                        </Table>
                      </CardBody>
                    </Card>
                  </Col>
            
                </Row>
              </div>
            </>
          );
       }else{
         return(
           <>
           </>
         )
       }
}

export default Tables;
