/*!

=========================================================
* Black Dashboard React v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import ModalLogin from "../components/ModalLogin.jsx";
import React from "react";
import Confetti from 'react-dom-confetti';
import './UserProfile.css';

import { provider } from "../components/Provider.js";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
  Table
} from "reactstrap";

class UserProfile extends React.Component {

  constructor(props) {
    super();
    this.state = {
      confettiActive: false,
      addingProject: false,
      userProjects: [],
      loading: false,
      projectRecommendation: [],
      lastProject: {},
      remarkProjectModal: false,
      projId: null,
      step: 'step1',
      config: {
        angle: 30,
        spread: "150",
        startVelocity: "30",
        elementCount: "120",
        dragFriction: "0.07",
        duration: "5000",
        stagger: 0,
        width: "30px",
        height: "30px",
        colors: ["#a864fd", "#29cdff", "#78ff44", "#ff718d", "#fdff6a"]
      }
    };

  }

  componentDidMount() {
    console.log('rrrrrrrrrrrrrrrrrrrrrrrr :');

    this.getFeatures().then(r => {
      console.log('rrrrrrrrrrrrrrrrrrrrrrrr :', r);
      if (r && r.length) this.setState({ features: r.map(el => Object.assign(el, { checked: false })) });
      // else this.setState({ features: [{ name: 'AAAA', id: 1 }, { name: 'BBB', id: 1 }].map(el => Object.assign(el, { checked: false })) });
    })
    if (localStorage.getItem('token')) {
      this.getUserProjects().then(r => {
        if (r && r.length) this.setState({ userProjects: r });
      })
    }

  }

  changeStep = (state) => {
    this.setState({ step: state });
  }

  changeProjId = (projId) => {
    this.setState({ projId: parseInt(projId) })
  }

  // markProject = (e) => {
  //   let mark = 5;
  //   if (mark > 3) {
  //     for (let i = 0; i < mark; i++) {
  //       this.fireConfetti(30 * i, i * 1000);
  //     }
  //   }
  // }

  onAfterAdd = ({ projId, mark, url, name }) => {
    this.setState({ lastProject: { name: name, url, mark, projId } });
    if (mark && mark > 3) {
      for (let i = 0; i < mark; i++) {
        this.fireConfetti(40 * i, i * 1000);
      }
    }
  }

  // markProject = (projId) => {
  //   if (projId) {
  //     let body = {
  //       "projectId": parseInt(projId),
  //       "featureIdsList": [1]
  //     }
  //     return provider({ url: 'Project/mark', method: 'POST', body, token: true })
  //   }
  // }


  openAddProjectDailog = () => {
    this.setState({ addingProject: true })
  }

  onFeaturesChecked = (e) => {
    if (e && e.target && e.target.name) {
      let name = e.target.name;
      let checked = e.target.checked;
      this.setState(state => ({ features: state.features.map(el => el.id == name ? Object.assign(el, { checked: checked }) : el) }))
    }
  }

  onCancelAdd = () => {
    this.setState({ addingProject: false });
  }

  onCancelRemark = () => {
    this.setState({ remarkProjectModal: false });
  }


  onProjectNameChange = (e) => {
    console.log('e.target.value :', e.target.value);
    this.setState({ projectName: e.target.value });
  }

  onGitChanged = (e) => {
    console.log('e.target.value :', e.target.value);
    this.setState({ git: e.target.value });
  }

  fireConfetti = (angle, timeout) => {
    setTimeout(() => {
      this.setState(state => ({ confettiActive: !state.confettiActive, config: { ...state.config, angle: angle } }));
    }, timeout);
    setTimeout(() => {
      this.setState(state => ({ confettiActive: !state.confettiActive }));
    }, timeout);
  }

  getFeatures = () => {
    return provider({ url: 'Feature', method: 'GET' });
  }

  getUserProjects = () => {
    if (localStorage.getItem('token')) return provider({ url: 'Project/usersProjects', method: 'GET', token: true });
  }





  setSelectedFeature = (e) => {
    let { name } = e.target;
    if (name) {
      let tmp = ({
        features: this.state.features.map(el => {
          if (el.id == name) {
            el.selected = !el.selected;
            el.className = el.selected ? 'btn--selected' : ''
            return el;
          }
          else {
            return el;
          }
        })
      })
      this.setState({ ...tmp })
    }
  }

  remarkProject = (e) => {
    console.log('remarkProject :', e.target.id);
    let { name, dataset, id } = e.target
    this.setState({ step: 'step2' });

    this.setState({ remarkProjectModal: true });

    this.changeProjId(id);
    // let { name, dataset, id } = e.target
    // this.markProject(e.target.id).then(r => {
    //   let mark = r;
    //   this.onAfterAdd({ projId: id, mark, url: dataset && dataset.url, name })
    //   this.getUserProjects().then(res => {
    //     this.setState(state => ({ userProjects: res }));
    //     if (mark && mark > 3) {
    //       for (let i = 0; i < mark - 2; i++) {
    //         this.fireConfetti(30 * i, i * 1000);
    //       }
    //     }
    //   })
    // })
  }


  getProjectInfo = (e) => {
    console.log('getProjectInfo :', e.target.id);
    let headers = {
      // 'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
    fetch("https://cleancodespontanapi.azurewebsites.net/api/Project/getMarkDetails/" + e.target.id,
      { headers: headers, })
      .then((resp) => resp.json()) // Transform the data into json
      .then((data) => {
        this.setState({
          projectRecommendation: data
        });
      });
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="8">
              <Card>
                <CardBody>
                  <h2 className="title">Don't wait mark your project now !</h2>
                </CardBody>

                <CardFooter className="my-card--footer">
                  {/* <Button className="btn-fill" onClick={this.markProject} color="primary" type="submit">
                  Mark project
                  </Button> */}
                  <Button className="btn-fill" onClick={this.openAddProjectDailog} color="primary" type="submit">
                    Add new project
                  </Button>

                </CardFooter>
              </Card>

              <Card>
                <Confetti className="myConfetti" active={this.state.confettiActive} config={this.state.config} />

                <CardHeader>
                  <h3 className="title">Uploaded projects</h3>
                </CardHeader>
                <CardBody className="uploaded-projects-list">
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                        <th>Project</th>
                        <th>Mark</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <TableRows data={this.state.userProjects} getInfo={this.getProjectInfo} onRemark={this.remarkProject}></TableRows>
                    </tbody>
                  </Table>
                </CardBody>
              </Card>

              <ProjectInfo data={this.state.projectRecommendation}></ProjectInfo>

            </Col>
            <Col md="4">
              <Card className="card-user">
                <CardBody>
                  <CardText />
                  <div className="author">
                    <div className="block block-one" />
                    <div className="block block-two" />
                    <div className="block block-three" />
                    <div className="block block-four" />
                    <img
                      alt="..."
                      className="avatar"
                      src={require("assets/img/emilyz.jpg")}
                    />
                    <h3 className="title">Last mark</h3>
                    <h3 className="title">{this.state.lastProject && this.state.lastProject.mark}</h3>
                  </div>
                  <div className="card-description">
                    {/* <p>Last rated project: {this.state.lastProject && this.state.lastProject.name}</p>
                    <p>URL: {this.state.lastProject && this.state.lastProject.url}</p> */}
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>

        <ModalLogin changeStep={this.changeStep} isOpen={this.state.loading} modalType="loading" />
        <ModalLogin changeProjId={this.changeProjId} changeStep={this.changeStep} modalTitle="Mark project" step={this.state.step} onFeaturesChecked={this.onFeaturesChecked} projId={this.state.projId} features={this.state.features} onAfterAdd={this.onAfterAdd} onCancelAdd={this.onCancelAdd} isOpen={this.state.addingProject} />
        <ModalLogin changeProjId={this.changeProjId} changeStep={this.changeStep} modalTitle="Check your progress let's remark project" step={this.state.step} projId={this.state.projId} onFeaturesChecked={this.onFeaturesChecked} features={this.state.features} onAfterAdd={this.onAfterAdd} onCancelAdd={this.onCancelRemark} isOpen={this.state.remarkProjectModal} />

      </>
    );
  }
}




const Feature = ({ data, onClick }) => {
  console.log('data :', data);
  if (data && data.length > 0) {
    const categorySelected = (e) => {
      onClick(e);
    }

    let el = (
      <ButtonGroup>
        {data.map(element => (
          <Button className={element.className} onClick={categorySelected} name={element.id} active={element.selected}>{element.name}</Button>
        )
        )}
      </ButtonGroup>
    )
    return (
      <div>
        <h5 className="title">Select code evaluation criteria</h5>
        <Row>
          <Col md="1"></Col>
          <Col md="5">
            {el}
          </Col>
        </Row>
      </div>
    )
  } else {
    return (
      <div>
        Brak danych
     </div>
    )
  }
}



const TableRows = ({ data, onRemark, getInfo }) => {
  return (
    <>
      {data.map(element => {
        return (<tr key={element.id}>
          <td>{element.name}</td>
          <td><strong>{element.mark}</strong></td>
          <td><Button name={element.name} data-url={element.url} id={element.id} onClick={onRemark}>Remark !</Button></td>
          <td><Button id={element.id} onClick={getInfo}><i id={element.id} className="tim-icons icon-notes" /></Button></td>
        </tr>)
      })}
    </>
  )
}

// "featureName": "string",
// "partialMark": 0,
// "guideline": "string",
// "argumentation": "string"

const ProjectInfo = ({ data }) => {
  var featuresDetails = data.featuresDetails;
  if (!featuresDetails ) { return (<></>) }
  let recomendations = (
    <div className="card-description">
      {console.log(featuresDetails)}
      {featuresDetails && featuresDetails.map(el => {
        return (
          <div key={el.featureName}>
            <h4>{el.featureName}</h4>
            {el.guideline}<br /><br />
            {el.argumentation}<br /><br />
          </div>
        )
      })}
    </div>
  )
  return (
    <Card className="card-user">
      <CardHeader>
        <h5 className="title">Recomendation</h5>
      </CardHeader>
      <CardBody>
        {recomendations}
      </CardBody>
    </Card>
  )
}

export default UserProfile;
