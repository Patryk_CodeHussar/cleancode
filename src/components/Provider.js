


//https://github.com/Astorn12/TestProjectForCleanCodeSpontan

export const provider = ({ url, method, body, token = false }) => {
  const apiUrl = 'https://cleancodespontanapi.azurewebsites.net/api/';
  let headers;
  if (token) {
    headers = {
      // 'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  } else {
    headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  }

  return fetch(`${apiUrl}${url}`, {
    method: method,
    mode: 'cors',
    headers: headers,
    body: JSON.stringify(body)
  })
    .then((response) => response.json())
}

