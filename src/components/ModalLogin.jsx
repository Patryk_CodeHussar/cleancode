/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState } from 'react';
import { Input, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import "../assets/css/ModalLogin.css"
import PacmanLoader from "react-spinners/PacmanLoader";
import Stepper from 'react-stepper-horizontal';
import { provider } from "../components/Provider.js";

const ModalLogin = (props) => {
  const {
    buttonLabel,
    modalTitle,
    className,
    modalType,
    isOpen,
    onCancelAdd,
    afterLogin,
    onAfterAdd,
    features,
    onFeaturesChecked,
    step,
    changeStep,
    projId,
    changeProjId
  } = props;

  //const [modal, setModal] = useState();
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [openedLogin, setOpenedLogin] = useState(false);
  //https://github.com/Astorn12/TestProjectForCleanCodeSpontan
  const [gitUrl, setGitUrl] = useState("https://github.com/Astorn12/TestProjectForCleanCodeSpontan");
  // const [projId, setProjId] = useState("");
  const [mark, setMark] = useState("");

  const [openedStep, setOpenedStep] = useState(isOpen);
  // const [step, setStep] = useState(props.step);

  // const [gitUrl, setGitUrl] = useState("");
  const [catArray, setCatArray] = useState([]);
  // const [isOpen,setisOpen] = useState(modal);
  const onGitUrlChanged = (e) => {
    console.log('e.target.value :', e.target.value);
    setGitUrl(e.target.value);
  }

  const addProject = () => {
    if (gitUrl) {

      changeStep('loading');
      let patt = new RegExp("((git|ssh|http(s)?)|(git@[\w\.]+))(:(//)?)([\w\.@\:/\-~]+)(\.git)(/)?");
      let res = patt.test(gitUrl);
      console.log('res :', res);
      res && provider({ url: 'Project/add', method: 'POST', body: { url: gitUrl }, token: true }).then(r => {
        //   this.setState({loading:false});
        console.log('r :', r);

        changeProjId(r.id);
        setTimeout(() => {
          changeStep('step2');
        }, 1000);
      })
    }
  }

  const markProject = () => {
    if (projId) {
      changeStep('loading');
      let featureIdsList = features.length > 0 ? features.filter(el => el.checked).map(el => el.id) : [1];
      let body = {
        "projectId": projId,
        "featureIdsList": featureIdsList.length > 0 ? featureIdsList : [1]
      }
      provider({ url: 'Project/mark', method: 'POST', body, token: true }).then(r => {
        console.log('r :', r);
        setMark(r);
        setTimeout(() => {
          changeStep('step3');
          onAfterAdd({ projId, mark: r })
        }, 1000);
      })
    }
  }


  const _onFeaturesChecked = (e) => {
    onFeaturesChecked(e);
  }

  const toggle = (e) => {
    setOpenedLogin(!openedLogin);
  }

  const _onCancelAdd = (e) => {
    onCancelAdd(e);
    changeStep('step1');
  }

  const setUserName = (e) => {
    console.log('e :', e.target.value);
    setName(e.target.value);
  }
  const setUserPassword = (e) => {
    console.log('e :', e.target.value);
    setPassword(e.target.value);
  }
  const login = () => {
    if (modalType == 'login') {
      console.log('name,password :', name, password);
      fetch('https://cleancodespontanapi.azurewebsites.net/api/Auth/login', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify({ username: name, password: password })
      })
        .then((response) => response.json())
        .then((data) => {
          console.log('Success:', data);
          localStorage.setItem('token', data.token);
          setName('');
          setPassword('')
          toggle();
          afterLogin();
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    } else {
      console.log('name,password :', name, password);
      fetch('https://cleancodespontanapi.azurewebsites.net/api/Auth/register', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify({ username: name, password: password })
      })
        .then((data) => {
          console.log('Success:');
          setName('');
          setPassword('')
          toggle();
          afterLogin();
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }

  }
  let mContent;
  if (modalType == 'login' || modalType == 'signIn') {
    mContent = (
      <div>
        <Button color="danger" className="login-btn" onClick={toggle}>{buttonLabel}</Button>
        <Modal isOpen={openedLogin} toggle={toggle} className={className} centered>
          <ModalHeader>{modalTitle}</ModalHeader>
          <ModalBody className="my_input">
            <Input className="my-input--margin" onChange={setUserName} placeholder="Podaj nazwę użytkownika" value={name}></Input>
            <Input type="password" className="my-input--margin" onChange={setUserPassword} placeholder="Podaj hasło" value={password}></Input>
          </ModalBody>
          <ModalFooter>
            <Button name="cancel" color="cancel" onClick={toggle}>Cancel</Button>
            <Button color="primary" onClick={login}>{modalType == 'login' ? "Log in" : "Sign in"}</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  } else if (step == 'loading') {
    mContent = (
      <Modal isOpen={isOpen} centered className="modal-container">
        {/* <ModalBody className="modal-loading"> */}
        <div className="loading-text">Loading ...</div>
        <div className="pac-loader">
          <PacmanLoader
            size={100}
            color={"#e14ecd"}
            loading={isOpen}
          />
        </div>

        {/* </ModalBody> */}
      </Modal>
    )
  } else if (step == 'step1') {
    mContent = (
      <Modal isOpen={isOpen} centered className="modal-container">
        <ModalHeader>{modalTitle}</ModalHeader>
        <ModalBody className="my_input">
          <Stepper size={48} steps={[{ title: 'GIT URL' }, { title: 'Categories' }, { title: 'Project mark' }]} activeStep={0} />
          <Input className="my-input--margin" onChange={onGitUrlChanged} placeholder="Enter your git repo url" value={gitUrl}></Input>
        </ModalBody>
        <ModalFooter className="modal-footer--custom">
          <Button color="cancel" className="btn-action" onClick={_onCancelAdd}>Cancel</Button>
          <Button color="primary" className="btn-action" onClick={addProject}>Add</Button>
        </ModalFooter>
      </Modal>
    )
  } else if (step == 'step2') {
    mContent = (
      <Modal isOpen={isOpen} centered className="modal-container">
        <ModalHeader>{modalTitle}</ModalHeader>
        <ModalBody className="my_input">
          <Stepper size={48} steps={[{ title: 'GIT URL' }, { title: 'Categories' }, { title: 'Project mark' }]} activeStep={1} />
          {/* <Feature data={this.state.features} onClick={this.setSelectedFeature}></Feature> */}
          <div className="features-list">
            {features.map(element => {
              return (
                <div key={element.id} className="title t-space">
                  <label className="l-space">
                    {element.name}&nbsp;&nbsp;
                  <input
                      name={element.id}
                      type="checkbox"
                      checked={element.checked}
                      onChange={_onFeaturesChecked} />
                  </label>

                </div>
              )
            })}
          </div>


        </ModalBody>
        <div className="modal-footer--custom">
          <Button color="cancel" className="btn-action" onClick={_onCancelAdd}>Cancel</Button>
          <Button color="primary" className="btn-action" onClick={markProject}>Mark</Button>
        </div>
      </Modal>
    )
  } else if (step == 'step3') {
    mContent = (
      <Modal isOpen={isOpen} centered >
        <ModalHeader>
          <div className="mark-text">
            {mark < 4 ? "Ouch so bad ..." : "Congratulations !"}
          </div>
        </ModalHeader>
        <ModalBody className="my_input">
          {/* <Stepper size={48} steps={[{ title: 'GIT URL' }, { title: 'Categories' }, { title: 'Project mark' }]} activeStep={2} /> */}
          <div className="mark">{mark}</div>
        </ModalBody>
        <ModalFooter className="modal-footer--custom">
          <Button color="primary" className="btn-action" onClick={_onCancelAdd}>Close</Button>
        </ModalFooter>
      </Modal>
    )
  }

  return (
    <>
      {mContent}
    </>
  );
}

export default ModalLogin;